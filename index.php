#!/usr/bin/php
<?php
/**
 * @author Steinsplitter / https://commons.wikimedia.org/wiki/User:Steinsplitter
 * @copyright 2016 GlobalUsageCount authors
 * @license http://unlicense.org/ Unlicense
 * @mirror by Ahecht / https://commons.wikimedia.org/wiki/User:Ahecht
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>

<?php
function i18nheader($hname) {
echo "                <a class=\"brand\" href=\"#\">$hname</a>".PHP_EOL.
     "                <div class=\"nav-collapse collapse\">".PHP_EOL.
     "                    <ul id=\"toolbar-right\" class=\"nav pull-right\">".PHP_EOL.
     "                    </ul>".PHP_EOL.
     "                </div>".PHP_EOL.
     "            </div>".PHP_EOL.
     "        </div>".PHP_EOL.
     "    </div>".PHP_EOL.
     "    <div class=\"container\"> ".PHP_EOL.
     "        ";
}

function i18nparser($url) {
    $con = curl_init();
    $to = 4;
    curl_setopt($con, CURLOPT_URL, $url);
    curl_setopt($con, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($con, CURLOPT_CONNECTTIMEOUT, $to);
    curl_setopt($con,CURLOPT_USERAGENT,'GlobalUsageCount interface parser, running on toollabs / tools.wmflabs.org/globalusagecount');
    $data = curl_exec($con);
    curl_close($con);
    return $data;
}
//Main globalusagecount URL
$gucURL = "https://globalusagecount.toolforge.org";

// Check main URL
ini_set('user_agent', $_SERVER['HTTP_USER_AGENT']);
$headers = @get_headers($gucURL); 

if($headers && strpos( $headers[0], '200')) { //redirect to main URL if it's working
    $newURL = $gucURL . $_SERVER['REQUEST_URI'];
    header("Location: " . $newURL); 
    echo "    <meta http-equiv=\"refresh\" content=\"; URL=\"" . $newURL . "\" />".PHP_EOL.
         "</head><body>".PHP_EOL.
         "Redirecting to <a href=\"" . $newURL . "\">" . $newURL . "</a>.".PHP_EOL.
         "</body></html>";
} else {
    echo "    <meta charset=\"utf-8\">".PHP_EOL.
         "    <title>GlobalUsageCount</title>".PHP_EOL.
         "    <link rel=\"stylesheet\" href=\"//tools-static.wmflabs.org/cdnjs/ajax/libs/twitter-bootstrap/2.3.2/css/bootstrap.min.css\">".PHP_EOL.
         "    <style>".PHP_EOL.
         "        body {".PHP_EOL.
         "            padding-top: 60px;".PHP_EOL.
         "        }".PHP_EOL.
         "    </style>".PHP_EOL.
         "</head>".PHP_EOL.
         "<body>".PHP_EOL.
         "    <div class=\"navbar navbar-fixed-top\">".PHP_EOL.
         "        <div class=\"navbar-inner\">".PHP_EOL.
         "            <div class=\"container\">".PHP_EOL;
    
    $getd2 = $_GET['lang'];
    if(preg_match("/^[a-z]{1,4}(-[a-z]{1,4}|)+$/",$getd2)) {
         $lang = htmlspecialchars($getd2);
    } else {
         $lang = "en";
    }
    $i18n = i18nparser('https://commons.wikimedia.org/w/index.php?title=Commons:User_scripts/GlobalUsageCount_i18n/'. $lang . '&action=raw&ctype=text');
    if (strpos($i18n, 'a') === false)
    {
        i18nheader("GlobalUsageCount");
        echo "Ooop :-(. No interface translation available for ". $lang . "." .
             "<a href=\"https://commons.wikimedia.org/w/index.php?title=Special:Translate&group=page-Commons%3AUser+scripts%2FGlobalUsageCount+i18n&language=". $lang ."&action=page&filter=\">".
             "Please help with the translation!</a>".PHP_EOL;
    }
    else
    {
      $i18nhe = preg_replace("/(.+\n*<!--header:|-->(.+[.\n]*)*)/", "", $i18n);
      i18nheader(htmlspecialchars($i18nhe));
      $i18ntr = preg_replace("/\<noinclude\>.+\n*\<\/noinclude\>/", "", $i18n);
      $esc= htmlspecialchars($i18ntr);
      echo preg_replace("/\n/", "<br>", $esc);
    }
    echo "<br><br>".PHP_EOL.
         "        <form class=\"input-prepend input-append\">".PHP_EOL.
         "            <span class=\"add-on\">File:</span>".PHP_EOL.
         "            <input type=\"text\" value=\"" . $lang . "\" name=\"lang\" id=\"lang\" class=\"hidden\" type=\"hidden\" style = \"display:none; visibility:hidden;\" />".PHP_EOL.
         "            <input type=\"text\" value=\"\" name=\"file\" id=\"fast\" class=\"input-medium appendedPrependedInput appendedInputButton\"/>".PHP_EOL.
         "            <button type=\"submit\" class=\"btn\">&#128269;</button>".PHP_EOL.
         "        </form>".PHP_EOL.
         "        <br/>".PHP_EOL;
    
    $getd = $_GET['file'];
    if (isset($getd)) {
        $tools_pw = posix_getpwuid(posix_getuid());
        $tools_mycnf = parse_ini_file($tools_pw['dir'] . "/replica.my.cnf");
        $db = new mysqli('commonswiki.labsdb', $tools_mycnf['user'], $tools_mycnf['password'], 'commonswiki_p');
        if ($db->connect_errno) {
            $error_text = "Failed to connect to the database: (" . $db->connect_errno . ") " . $db->connect_error;
            echo "        <p class=\"text-error\">". $error_text . "</p>";
        } else {
            $r = $db->query('SELECT COUNT(gil_page) AS count FROM globalimagelinks WHERE gil_to  = "' . str_replace(" ", "_", $db->real_escape_string($getd)) . '" LIMIT 1;');
            echo "        <p><big>" . htmlspecialchars($getd) . "</big></p>".PHP_EOL;
            if (isset($r)) {
                $row = $r->fetch_assoc();
                echo "        <div style = \"text-align:center; font-size: 150px; padding: 0.5em; border-color:Black; border-style:solid; border-width:1pt\">" . $row['count'] ."</div>".PHP_EOL;
                $r->close();
            }
            $db->close();
        }
        unset($tools_mycnf, $tools_pw);
    }
    
    echo "    </div>".PHP_EOL;
}
?>
</body>
</html>